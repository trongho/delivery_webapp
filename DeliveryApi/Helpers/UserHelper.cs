﻿using DeliveryApi.Entites;
using DeliveryApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Helpers
{
    public class UserHelper
    {
        public static List<UserModel> CovertUsers(List<User> users)
        {
            var userModels = users.ConvertAll(user => new UserModel
            {
                UserID = user.UserID,
                UserName = user.UserName,
                Adress = user.Adress,
                Phone=user.Phone,
                Email = user.Email,
                FullName = user.FullName,
                Password = user.Password,
                PasswordSalt=user.PasswordSalt,
                CreatedUserID = user.CreatedUserID,
                CreatedDate = user.CreatedDate,
                UpdatedUserID = user.UpdatedUserID,
                UpdatedDate = user.UpdatedDate,
                Active=user.Active,
                Blocked=user.Blocked,
                Role=user.Role,
            });

            return userModels;
        }
    }
}
