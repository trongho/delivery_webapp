﻿using DeliveryApi.Entites;
using DeliveryApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Helpers
{
    public class LocationsHelper
    {
        public static List<LocationsModel> Covert(List<Locations> entrys)
        {
            var models = entrys.ConvertAll(sc => new LocationsModel
            {
                Latitude=sc.Latitude,
                Longitude=sc.Longitude,
            });

            return models;
        }
    }
}
