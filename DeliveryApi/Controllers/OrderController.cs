﻿using DeliveryApi.Entites;
using DeliveryApi.Helpers;
using DeliveryApi.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService service;

        public OrderController(IOrderService service)
        {
            this.service = service;
        }

        [HttpPost]
        [Route("UploadFile")]
        public async Task<IActionResult> UploadFile([FromForm] IFormFile file)
        {
            var entrys = service.UploadFile(file);
            if (entrys == null)
            {
                return NotFound();
            }
            return Ok(entrys);
        }


        [HttpGet]
        [Route("GetFile/{fileName}")]
        public async Task<IActionResult>DownloadFile(String fileName)
        {
            var entrys = service.GetFile(fileName);
            if (entrys == null)
            {
                return NotFound();
            }
            return Ok(entrys);
        }

        [HttpGet]
        [Route("UploadFile2")]
        public async Task<IActionResult> UploadFile2([FromForm] IFormFile file)
        {
            
            var entrys = service.UploadFile2(file);
            if (entrys == null)
            {
                return NotFound();
            }
            return Ok(entrys);
        }


        [HttpGet]
        [Route("Import")]
        public async Task<IActionResult> Import()
        {
            var entrys = service.ImportAsync();
            if (entrys == null)
            {
                return NotFound();
            }

            var models = OrderHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpGet]
        [Route("Export")]
        public async Task<IActionResult> Export()
        {
            var result = service.Export();
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }

            var models = OrderHelper.Covert(entrys);

            return Ok(models);
        }


        [HttpGet]
        [Route("GetLngLat/{Adress}")]
        public IActionResult GetLngLat(String adress)
        {
            var entrys = service.GetLngLat(adress);
            if (entrys == null)
            {
                return NotFound();
            }
            return Ok(entrys);
        }

        [HttpGet]
        [Route("GetKilometers/{StartPoint}/{StopPoint}/{Vehice}")]
        public IActionResult GetKilometers(String StartPoint, String StopPoint, String Vehice)
        {
            var entrys = service.GetKilometers(StartPoint, StopPoint, Vehice);
            if (entrys == null)
            {
                return NotFound();
            }
            return Ok(entrys);
        }

        [HttpGet]
        [Route("GetByID/{orderId}")]
        public async Task<IActionResult> GetUnderId(String orderId)
        {
            var entrys = await service.GetUnderId(orderId);
            if (entrys == null)
            {
                return NotFound();
            }

            var models = OrderHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Order order)
        {

            var entrys = await service.Create(order);

            return CreatedAtAction(
                 nameof(Get), new { id = order.OrderID }, order);
        }

        [HttpPut]
        [Route("Put/{orderId}")]
        public async Task<IActionResult> Update(String orderId, [FromBody] Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (orderId != order.OrderID)
            {
                return BadRequest();
            }

            await service.Update(orderId, order);

            return new NoContentResult();
        }

        [HttpPatch]
        [Route("Patch/{orderId}")]
        public async Task<IActionResult> Patch(String orderId, [FromBody] JsonPatchDocument<Order> order)
        {
            try

            {
                //nodes collection is an in memory list of nodes for this example
                var entrys = await service.GetUnderId(orderId);
                if (entrys == null)
                {
                    return BadRequest();
                }
                order.ApplyTo(entrys[0], ModelState);//result gets the values from the patch request

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        [HttpPut]
        [Route("Ended/{OrderID}")]
        public async Task<IActionResult> Ended(String OrderID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await service.Ended(OrderID);

            return new NoContentResult();
        }

        [HttpPut]
        [Route("Cancel/{OrderID}")]
        public async Task<IActionResult> Cancel(String OrderID, [FromBody] Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await service.Cancel(OrderID, order);

            return new NoContentResult();
        }

        [HttpPut]
        [Route("StartOrder/{OrderID}")]
        public async Task<IActionResult> Start(String OrderID, [FromBody] Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await service.Start(OrderID, order);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{orderId}")]
        public async Task<IActionResult> Delete(String orderId)
        {
            var entrys = await service.Delete(orderId);
            if (entrys == null)
            {
                return NotFound();
            }

            return Ok(entrys);
        }


        [HttpGet]
        [Route("lastID")]
        public async Task<IActionResult> GetLastUserId()
        {
            var orderID = await service.GetLastID();
            if (orderID == null)
            {
                return NotFound();
            }

            return Ok(orderID);
        }
    }
}
