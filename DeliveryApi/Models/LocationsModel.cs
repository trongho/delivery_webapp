﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Models
{
    public class LocationsModel
    {
        public String Latitude { get; set; }
        public String Longitude { get; set; }
    }
}
