﻿using DeliveryApi.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Interfaces
{
    public interface IOrderResultService
    {
        Task<List<OrderResult>> GetAll();
    }
}
