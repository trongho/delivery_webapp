﻿using DeliveryApi.Entites;
using DeliveryApi.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Services
{
    public class WarehouseService : IWarehouseService
    {
        private readonly DeliveryDbContext context;

        public WarehouseService(DeliveryDbContext context)
        {
            this.context = context;
        }

        public async Task<bool> Create(Warehouse warehouse)
        {
            var entrys = context.Warehouses.AddAsync(warehouse);
            context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = context.Warehouses.FirstOrDefault(o => o.WarehouseID.Equals(id));
            context.Warehouses.Remove(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<List<Warehouse>> GetAll()
        {
            var entrys = context.Warehouses;
            return await entrys.ToListAsync();
        }

        public async Task<List<Warehouse>> GetUnderId(string id)
        {
            var entrys = context.Warehouses.Where(u => u.WarehouseID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, Warehouse warehouse)
        {
            var entrys = context.Warehouses.FirstOrDefault(o => o.WarehouseID.Equals(id));
            context.Entry(entrys).CurrentValues.SetValues(warehouse);
            context.SaveChanges();
            return true;
        }
    }
}
