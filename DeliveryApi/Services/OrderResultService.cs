﻿using DeliveryApi.Entites;
using DeliveryApi.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApi.Services
{
    public class OrderResultService: IOrderResultService
    {
        private readonly DeliveryDbContext context;

        public OrderResultService(DeliveryDbContext context)
        {
            this.context = context;
        }

        public async Task<List<OrderResult>> GetAll()
        {
            var entrys = context.OrderResults;
            return await entrys.ToListAsync();
        }
    }
}
