﻿using DeliveryApi.Entites;
using DeliveryApi.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace DeliveryApi.Services
{

    public class OrderService : IOrderService
    {
        private readonly DeliveryDbContext context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public OrderService(DeliveryDbContext context, IHostingEnvironment hostingEnvironment)
        {
            this.context = context;
            _hostingEnvironment = hostingEnvironment;
        }


        public async Task<bool> Create(Order order)
        {
            order.EndPoint = GetLngLat(order.CustomerAdress);
            var entrys = context.Orders.AddAsync(order);
            context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            context.Orders.Remove(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> Ended(string id)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            if (entrys != null)
            {
                entrys.Status = "Ended";
                entrys.Result = "Success";
                entrys.StopTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                context.Entry(entrys).CurrentValues.SetValues(entrys);
                context.SaveChanges();
            }
            return true;
        }

        public async Task<string> GetFile(String filename)
        {
            string result = "";
            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),"aaaaaaaa",filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            result = "true";
            return result;
        }

        public string Export()
        {
            string rootFolder = _hostingEnvironment.WebRootPath;
            string fileName = @"importOrder.xlsx";
            string result = "";
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));

            using (ExcelPackage package = new ExcelPackage(file))
            {

                List<Order> orders = context.Orders.ToList();

                if (package.Workbook.Worksheets.Count>10)
                {
                    result = "More more worksheet";
                }
                else
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
                    int totalRows =orders.Count;

                    int i = 0;
                    for (int row = 5; row <= totalRows+1; row++)
                    {
                        worksheet.Cells[row, 2].Value = orders[i].OrderID;
                        i++;
                    }

                    result = "GoodsLine list has been exported successfully";
                }
                package.Save();

            }

            return result;
        }

        public async Task<List<Order>> GetAll()
        {
            var entrys = context.Orders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastID()
        {
            var orderID = context.Orders.OrderByDescending(x => x.OrderID).Take(1).Select(x => x.OrderID).ToList().FirstOrDefault();
            return orderID;
        }

        public async Task<List<Order>> GetUnderId(string id)
        {
            var entry = context.Orders.Where(u => u.OrderID.Equals(id));
            return await entry.ToListAsync();
        }


        public List<Order> ImportAsync()
        {
            string rootFolder = _hostingEnvironment.WebRootPath;
            string fileName = @"importOrder.xlsx";
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));


            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = workSheet.Dimension.Rows;
                List<Order> orders = new List<Order>();
                int orderId = getOrderID();
                for (int i = 5; i <= totalRows; i++)
                {
                    String selected = workSheet.Cells[i, 1].Text.Trim();
                    if (selected.Equals("x"))
                    {
                        orders.Add(new Order
                        {
                            OrderID = $"{orderId:D10}",
                            RequiredDate = DateTime.Parse("01-01-2021"),
                            WarehouseID = workSheet.Cells[i, 6].Text.Trim(),
                            ShippingMethod = workSheet.Cells[i, 11].Text.Trim(),
                            OrderValue = Decimal.Parse(workSheet.Cells[i, 12].Text.Trim()),
                            BussinesStaffID = workSheet.Cells[i, 13].Text.Trim(),
                            Note = workSheet.Cells[i, 14].Text.Trim(),
                            ShippingType = "",
                            ShippedDate = null,
                            Employee1 = "",
                            Employee2 = "",
                            OrderFee = 0,
                            StartTime = null,
                            StopTime = null,
                            Mass = 0,
                            Kilometers = 0,
                            Result = "",
                            Reason = "",
                            CustomerAdress = workSheet.Cells[i, 8].Text.Trim(),
                            CustomerName = workSheet.Cells[i, 7].Text.Trim(),
                            CustomerPhone = workSheet.Cells[i, 10].Text.Trim(),
                            CustomerContact = workSheet.Cells[i, 9].Text.Trim(),
                            StartPoint = "",
                            EndPoint = GetLngLat(workSheet.Cells[i, 8].Text.Trim()),
                            Status = "New",
                            
                        });
                        orderId++;
                    }
                    //remove exist record
                    //foreach (Order order in orders.ToList())
                    //{
                    //    if (context.Orders.Any(u => u.OrderID.Equals(order.OrderID)))
                    //    {
                    //        orders.Remove(order);
                    //    }
                    //}
                }
                context.Orders.AddRange(orders);
                context.SaveChanges();
                return orders;
            }
        }

        public int getOrderID()
        {
            int orderID =0;
            String lastID = context.Orders.OrderByDescending(x => x.OrderID).Take(1).Select(x => x.OrderID).ToList().FirstOrDefault();
            if (lastID ==null)
            {
                //orderID =$"{1:D10}";
                orderID = 1;
            }
            else
            {
                orderID = int.Parse(lastID) + 1;
                //orderID =$"{int.Parse(orderID):D10}";
            }
            return orderID;
        }

        public async Task<List<Order>> Patch(string id)
        {
            var entry = context.Orders.Where(u => u.OrderID.Equals(id));
            return await entry.ToListAsync();
        }

        public async Task<bool> Update(string id, Order order)
        {
            
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            context.Entry(entrys).CurrentValues.SetValues(order);
            context.SaveChanges();
            return true;
        }

        public async Task<string> UploadFile(IFormFile file)
        {
            string path = Path.Combine(_hostingEnvironment.WebRootPath, "ExcelFile/" + file.FileName);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
 
            }
            return _hostingEnvironment.WebRootPath+"ExcelFile/" + file.FileName;
        }

        public async Task<bool> Cancel(string id,Order order)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
                order.Status = "Cancel";
                order.Result = "Failure";
                context.Entry(entrys).CurrentValues.SetValues(order);
               context.SaveChanges();
            return true;
        }

        public String GetLngLat(String adress)
        {
            
            var fileJsonString = "";
            String firstString = "https://rsapi.goong.io/geocode?address=";
            String threeString = "&api_key=";
            String APiKey = "rDCEpOhGmuPf78dMFbiLh7jNEzeHaJEAj2qVM9ns";
            var uri = firstString + adress + threeString + APiKey;
            var json="";
            String lnglat = "";
            using (var client = new WebClient())
            {
                json = client.DownloadString(uri);               
            }

            dynamic data = JsonConvert.DeserializeObject(json);
            String lng = data.results[0].geometry.location.lng;
            String lat = data.results[0].geometry.location.lat;
            lnglat = lng + ","+lat;
            //var geometry = results["geometry"].Children();
            //var location = geometry["location"].Children();

            //foreach (var item in location)
            //{
            //    var lng = location["lng"].Value<string>();
            //    var lat = location["lat"].Value<string>();

            //    lnglat = lng + lat;
            //}
            

            return lnglat;
        }

        public async Task<bool> Start(string id, Order order)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            order.Status = "Started";
            order.StartTime =DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            context.Entry(entrys).CurrentValues.SetValues(order);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> Pause(string id, Order order)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            order.Status = "Pause";
            order.StopTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            context.Entry(entrys).CurrentValues.SetValues(order);
            context.SaveChanges();
            return true;
        }

        public Decimal GetKilometers(string startPoint, string endPoint,string vehice)
        {
            String firstString = "https://rsapi.goong.io/DistanceMatrix?origins=";
            String threeString = "&destinations=";
            String fiveString = "&vehicle=";
            String sevenString = "&api_key=";
            String APiKey = "rDCEpOhGmuPf78dMFbiLh7jNEzeHaJEAj2qVM9ns";
            var uri = firstString + startPoint + threeString + endPoint + fiveString + vehice + sevenString + APiKey;

            var json = "";
            using (var client = new WebClient())
            {
                json = client.DownloadString(uri);
            }

            dynamic data = JsonConvert.DeserializeObject(json);
            Decimal kilometers = data.rows[0].elements[0].distance.value;

            return kilometers;

        }

        public async Task<string> UploadFile2(IFormFile file)
        {
            string FILEPATH = @"d:\FORD\data\Book1.xlsx";
            var fileClient = new FileInfo(FILEPATH);

            string path = Path.Combine(_hostingEnvironment.WebRootPath, "ExcelFile/" + fileClient.Name);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            ImportAsync();
            return "http://localhost:5000/ExcelFile/" + file.FileName;
        }
    }
}
